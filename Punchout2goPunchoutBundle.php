<?php

namespace Punchout2go\Bundle\PunchoutBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Punchout2go\Bundle\PunchoutBundle\DependencyInjection\Punchout2goPunchoutExtension;

class Punchout2goPunchoutBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getContainerExtension()
    {
        if (!$this->extension) {
            $this->extension = new Punchout2goPunchoutExtension();
        }

        return $this->extension;
    }
}
